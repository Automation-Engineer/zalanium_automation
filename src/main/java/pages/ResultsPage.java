package pages;

import elements.ResultsPageElements;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import utility.FilterMethod;
import utility.WaitUntil;

import java.util.*;

import static org.openqa.selenium.By.*;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.expectThrows;


public class ResultsPage {

    WebDriver clientDriver;

    public WaitUntil waitUntil;

    ResultsPageElements resultsPageElements = new ResultsPageElements();

    static final Logger logger = LogManager.getLogger(ResultsPage.class);

    public FilterMethod filterMethod;

    public ResultsPage(WebDriver clientDriver) {
        this.clientDriver = clientDriver;
        waitUntil = new WaitUntil(clientDriver);
        filterMethod = new FilterMethod(clientDriver);

    }

    public void seeAvailableRoomsMethod() {
        clientDriver.findElement(resultsPageElements.getSeeAvailableRooms()).click();

        waitUntil.explicitWaitUntilPageLoad();

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getChangeHotel());
    }

    public void recommendedMethod() {

        waitUntil.explicitWaitUntilPageLoad();

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getRecommended());

        clientDriver.findElement(resultsPageElements.getRecommended()).isDisplayed();

        clientDriver.findElement(resultsPageElements.getRecommended()).click();
    }

    public void distanceMethod() {

        waitUntil.explicitWaitUntilPageLoad();

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getDistance());

        clientDriver.findElement(resultsPageElements.getDistance()).isDisplayed();

        clientDriver.findElement(resultsPageElements.getDistance()).click();

        clientDriver.findElement(resultsPageElements.getDistance()).click();
    }

    public void reviewScoreMethod() {

        waitUntil.explicitWaitUntilPageLoad();

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getReviewScore());

        clientDriver.findElement(resultsPageElements.getReviewScore()).isDisplayed();

        clientDriver.findElement(resultsPageElements.getReviewScore()).click();

        clientDriver.findElement(resultsPageElements.getReviewScore()).click();
    }

    public void starRatingMethod() {

        waitUntil.explicitWaitUntilPageLoad();

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getStarRating());

        clientDriver.findElement(resultsPageElements.getStarRating()).isDisplayed();

        clientDriver.findElement(resultsPageElements.getStarRating()).click();

        clientDriver.findElement(resultsPageElements.getStarRating()).click();
    }

    public void lowestPriceMethod() throws InterruptedException {

        waitUntil.explicitWaitUntilPageLoad();

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getLowestPrice());

        clientDriver.findElement(resultsPageElements.getLowestPrice()).isDisplayed();

        clientDriver.findElement(resultsPageElements.getLowestPrice()).click();

        clientDriver.findElement(resultsPageElements.getLowestPrice()).click();

        Thread.sleep(1000);
    }

    public void hotelNameSearchMethod() {

        waitUntil.explicitWaitUntilPageLoad();

        clientDriver.findElement(resultsPageElements.getHotelNameSearch()).click();

        clientDriver.findElement(resultsPageElements.getHotelNameSearch()).click();

        clientDriver.findElement(resultsPageElements.getHotelNameSearch()).sendKeys(Keys.ENTER);

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getHotelNameLoad());

        String name = clientDriver.findElement(resultsPageElements.getHotelNameLoad()).getText();

        System.out.println(name);

        waitUntil.explicitWaitUntilVisibility(resultsPageElements.getClearAllFilters());

        clientDriver.findElement(resultsPageElements.getClearAllFilters()).click();
    }

    public boolean breakFastService(String checkBox)
    {
        try
        {
            List<WebElement> more = clientDriver.findElements(By.linkText("...More"));

            System.out.println(more.size());

            for (WebElement moreElement : more)
            {
                moreElement.click();

                String r = checkBox.trim().replaceAll("[0-9,),(]", "").trim();

                waitUntil.explicitWaitUntilVisibility(resultsPageElements.getAmenitiesBox());

                List<WebElement> element = clientDriver.findElements(resultsPageElements.getAmenitiesBox());

                for(WebElement ele : element)
                {
                    String collectString = ele.getText();

                    if (collectString.toLowerCase().contains(r.toLowerCase()))
                    {
                        System.out.println("Condition Matched : "+r);

                    }
                    else
                    {
                        System.out.println("Condition not Matched : "+r);
                    }
                }
                waitUntil.fluentWaitMethod(resultsPageElements.getCloseButton()).click();
            }
        } catch (Exception e)
        {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public boolean validatePopularFilter() {
        try {

            Elements Checkboxes = filterMethod.getElement(resultsPageElements.getPopularFilter());

            for (Element checkbox : Checkboxes)
            {
                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                if (checkbox.text().contains("Free Cancellation")) {
                    if (!verifyPopularFilter(resultsPageElements.getFreeCancellation()))
                        return false;
                } else if (checkbox.text().contains("Free breakfast")) {
                    if (!verifyPopularFilter(resultsPageElements.getFreeBreakFast()))
                        return false;
                } else if (checkbox.text().contains("Bellboy service")) {
                    if (!verifyPopularFilter(resultsPageElements.getBellByService()))
                        return false;
                } else if (checkbox.text().contains("5 Star")) {
                    if (!verifyPopularFilter(resultsPageElements.getFiveStar()))
                        return false;

                    List<WebElement> star = clientDriver.findElements(resultsPageElements.getFiveStar());

                    System.out.println(star.size());
                } else if (checkbox.text().contains("Library")) {
                    try {
                        if (!verifyPopularFilter(resultsPageElements.getLibrary()))
                            return false;

                    } catch (Exception e) {
                        System.out.println(e.getStackTrace());
                    }

                } else if (checkbox.text().contains("Breakfast service")) {
                    try {
                        if (!breakFastService(checkbox.text()))
                            return false;
                    } catch (Exception e) {
                        System.out.println(e.getStackTrace());
                    }
                }
                filterMethod.selectCheckBox(checkbox);
                waitUntil.explicitWaitUntilPageLoad();
                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean verifyPopularFilter(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {

                    return false;
                }

                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;

    }

    public boolean verifyContentSource(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {

                    return false;
                }

                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateContentSource() {
        try {
            Elements Checkboxes = filterMethod.getElement(resultsPageElements.getContentSource());

            for (Element checkbox : Checkboxes)
            {
                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                if (checkbox.text().contains("Expedia"))
                {
                    if (!verifyContentSource(resultsPageElements.getExpedia()))
                        return false;
                } else if (checkbox.text().contains("Hotelbeds"))
                {
                    if (!verifyContentSource(resultsPageElements.getHotelBeds()))
                        return false;
                } else if (checkbox.text().contains("Priceline"))
                {
                    if (!verifyContentSource(resultsPageElements.getPriceLine()))
                        return false;
                }
                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean verifyDistanceFromCenter(String checkBoxText) {
        try {
            int i = 1;
            int totalPages = filterMethod.getTotalPages();

            logger.info("Total Pages is " + totalPages);
            do {
                i++;

                String[] range = (checkBoxText.replaceAll("[^\\d].{1,4}", "-").trim().split("--"));

                int startMiles = Integer.parseInt(range[0]);

                int endMiles = Integer.parseInt(range[1]);

                List<WebElement> miles = clientDriver.findElements(resultsPageElements.getMiles());

                for (WebElement ele : miles)
                {
                    String milesValue = ele.getText();

                    String r = milesValue.replaceAll("[|,\\s,a-z,A-Z]", "").trim();

                    System.out.println(Double.parseDouble(r));

                    if (!(startMiles <= Double.parseDouble(r) && endMiles >= Double.parseDouble(r))) {
                        return false;
                    }
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean validateDistanceFromCenter() {
        try {
            Elements Checkboxes = filterMethod.getElement(resultsPageElements.getDistanceFromCenter());

            for (Element checkbox : Checkboxes) {
                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                verifyDistanceFromCenter(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean avgPricePerNight(String checkBoxName) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;

                String[] r = checkBoxName.replaceAll("[\\n,a-z,A-z]", "").trim().replaceAll("[(,),\\s]", "-").trim().replace("---", "-").split("-");

                int initialPrice = Integer.parseInt(r[0]);

                int finalPrice = Integer.parseInt(r[1]);

                List<WebElement> element = clientDriver.findElements(resultsPageElements.getPriceText());

                for (WebElement ele : element)
                {
                    String text = ele.getText();

                    String rate = text.replaceAll("[^\\d,.,\\s]", "").trim();

                    System.out.println(Double.parseDouble(rate));

                    if (!(initialPrice <= Double.parseDouble(rate) && finalPrice >= Double.parseDouble(rate))) {
                        return false;
                    }
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateAvgPricePerNight() {
        try {
            Elements Checkboxes = filterMethod.getElement(resultsPageElements.getAvgPricePerNight());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                avgPricePerNight(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public void verifyPriceRangeSlide() {
        try {
            clientDriver.findElement(resultsPageElements.getSetOwnBudget()).click();
            Thread.sleep(1000);
            String priceRangeSlide = filterMethod.readPriceRangeSlide();
            filterMethod.slidePriceRange(priceRangeSlide);
            waitUntil.explicitWaitUntilPageLoad();
            Thread.sleep(2000);
            assertTrue(verifyPriceRangeResult());

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }

    public boolean verifyPriceRangeResult() {
        try {
            waitUntil.explicitWaitUntilPageLoad();
            ArrayList<Double> slidePrice = filterMethod.getSlidePrice();
            ArrayList<Double> resultPrice = getResultPrice();
            if (Collections.min(resultPrice) >= Collections.min(slidePrice)
                    && Collections.max(resultPrice) <= Collections.max(slidePrice))
                return true;
        } catch (Exception e) {
            logger.info(e);
        }
        return false;
    }

    public ArrayList<Double> getResultPrice() {
        try {
            ArrayList<Double> allPrice = new ArrayList<>();
            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                List<WebElement> priceList = waitUntil.fluentWaitMethodFindElements(
                        resultsPageElements.getPriceRatioSection());
                for (WebElement priceElement : priceList) {
                    String price = priceElement.findElement(By.cssSelector(".price_secondary_color.price_view"))
                            .getText().replaceAll("[^\\d+.\\d{2}]", "");
                    logger.info("" + price);
                    allPrice.add(Double.parseDouble(price));
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(resultsPageElements.getHotelPagination())
                            .findElement(By.linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

            return allPrice;
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    public boolean healthSafety() {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    ArrayList<WebElement> elements = (ArrayList<WebElement>) clientDriver.findElements(resultsPageElements.getHealthAndSafetyIcon());
                    {
                        elements.forEach(ele -> ele.isDisplayed());
                    }
                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateHealthSafety() {
        try {
            Elements Checkboxes = filterMethod.getElement(resultsPageElements.getHealthAndSafety());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                if (checkbox.text().contains("Health & Safety")) {
                    healthSafety();
                }
                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean propertyCategory(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validatePropertyCategory() {
        try {
            Elements Checkboxes = filterMethod.getElement(resultsPageElements.getPropertyCategory());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                propertyCategory(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean themesTypes(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateThemesTypes() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getThemes());

            if (Checkboxes.isEmpty()) {
                clientDriver.findElement(resultsPageElements.getNotExpand()).click();
            }
            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                themesTypes(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean rateType(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateRateType() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getRateTypes());

            if (Checkboxes.isEmpty())
            {
                clientDriver.findElement(resultsPageElements.getNotExpand()).click();
            }
            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                rateType(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean rateCheck(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateRateCheck() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getRateCheck());

            if (Checkboxes.isEmpty()) {
                clientDriver.findElement(resultsPageElements.getNotExpand()).click();
            }
            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                rateCheck(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean neighBorHood(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateNeighBorHood() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getNeighBorHood());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                neighBorHood(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
            filterMethod.expandShowAll(filterMethod.getExpand(resultsPageElements.getNeighBorHood()));

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean bedPreferences(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateBedPreferences() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getBedPreferences());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                bedPreferences(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
            filterMethod.expandShowAll(filterMethod.getExpand(resultsPageElements.getBedPreferences()));

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean hotelAmenities(By elementPath) {
        try {

            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;
                try {
                    List<WebElement> flightContList = waitUntil.fluentWaitMethodFindElements(resultsPageElements.getHotelContinue());

                    for (WebElement element : flightContList)

                        element.findElement(elementPath);

                } catch (Exception e) {
                    return false;
                }
                if (i <= totalPages) {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateHotelAmenities() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getHotelAmenities());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                hotelAmenities(resultsPageElements.getHotelNameLoad());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
            filterMethod.expandShowAll(filterMethod.getExpand(resultsPageElements.getHotelAmenities()));

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean hotelChains(String checkBox) {
        try {

            int i = 1;

            int totalPages = filterMethod.getTotalPages();

            logger.info("Total Pages is " + totalPages);

            do {
                i++;
                try
                {
                    waitUntil.explicitWaitUntilVisibility(resultsPageElements.getHotelNameLoad());

                } catch (Exception e)
                {
                    return false;
                }
                if (i <= totalPages)
                {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validateHotelChains() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getHotelChains());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                hotelChains(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
            filterMethod.expandShowAll(filterMethod.getExpand(resultsPageElements.getHotelChains()));

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean validateHotelBrands() {
        try {

            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getHotelChains());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                hotelChains(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
            filterMethod.expandShowAll(filterMethod.getExpand(resultsPageElements.getHotelChains()));

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean propertyAccessibility(String checkbox) {
        try {

            int i = 1;

            int totalPages = filterMethod.getTotalPages();

            logger.info("Total Pages is " + totalPages);
            do {
                i++;

                List<WebElement> more = clientDriver.findElements(By.linkText("...More"));

                System.out.println(more.size());

                for (WebElement moreElement : more)
                {
                    waitUntil.scrollView(moreElement);

                    moreElement.click();

                    String r = checkbox.trim().replaceAll("[0-9,),(]", "").trim();

                    waitUntil.explicitWaitUntilVisibility(resultsPageElements.getAmenitiesBox());

                    List<WebElement> element = clientDriver.findElements(resultsPageElements.getAmenitiesBox());

                    for(WebElement ele : element)
                    {
                        String collectString = ele.getText();

                        if (collectString.toLowerCase().contains(r.toLowerCase()))
                        {
                            System.out.println("Condition Matched : "+r);

                        }
                        else
                        {
                            System.out.println("Condition not Matched : "+r);
                        }
                    }
                    waitUntil.fluentWaitMethod(resultsPageElements.getCloseButton()).click();
                }
                if (i <= totalPages)
                {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();

                    waitUntil.explicitWaitUntilPageLoad();

                    Thread.sleep(2000);
                }
            } while (i <= totalPages);

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public boolean validatePropertyAccessibility() {
        try {
            Elements Checkboxes = filterMethod.expandGetElement(resultsPageElements.getPropertyAccessibility());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                propertyAccessibility(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
            filterMethod.expandShowAll(filterMethod.getExpand(resultsPageElements.getPropertyAccessibility()));

        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean starRating(String checkBox) {
        try {
            int i = 1;
            int totalPages = filterMethod.getTotalPages();
            logger.info("Total Pages is " + totalPages);
            do {
                i++;

                List<WebElement> elements = clientDriver.findElements(resultsPageElements.getHotelNameLoad());

                for (WebElement ele : elements)
                {
                    List<WebElement> list = ele.findElements(resultsPageElements.getIconStar());

                    System.out.println(list.size());

                    Integer a = list.size();

                    Integer b = Integer.parseInt(checkBox);

                    if (!(a == b))
                    {
                        return false;
                    }
                }
                if (i <= totalPages)
                {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            }
            while (i <= totalPages);
        } catch (Exception e) {
            logger.info(e);

            return false;
        }
        return true;
    }

    public boolean validateStarRating() {
        try {
            Elements Checkboxes = filterMethod.starRatingFilterGetElement(resultsPageElements.getStarRatingFilter());

            for (Element checkbox : Checkboxes) {

                System.out.println(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(3000);

                starRating(checkbox.text());

                filterMethod.selectCheckBox(checkbox);

                waitUntil.explicitWaitUntilPageLoad();

                Thread.sleep(2000);
            }
        } catch (Exception e) {
            logger.info(e);
        }
        return true;
    }

    public boolean guestRating()
    {
        try
        {
            int i = 1;

            int totalPages = filterMethod.getTotalPages();

            logger.info("Total Pages is " + totalPages);
            do {
                i++;

                if(i==2)
                {
                    if (clientDriver.findElement(By.xpath("//h3[text()='Guest Rating']")).isDisplayed()) ;
                    {
                        WebElement ele = clientDriver.findElement(By.cssSelector(".vue-slider-dot-handle"));

                        Actions builder = new Actions(clientDriver);

                        builder.dragAndDropBy(ele, 50, 0).build().perform();

                        waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelNameLoad());
                    }
                }

                String filterValue = clientDriver.findElement(resultsPageElements.getGetFilterText()).getText();

                String[] s = filterValue.replaceAll("[^\\d+.\\d{2}]","-").trim().split("--");

                Double alter = Double.parseDouble(s[0].trim());

                Double better = Double.parseDouble(s[1]);

                List<WebElement> elements = clientDriver.findElements(resultsPageElements.getImageValue());

                for (WebElement elem : elements)
                {
                    Double ele = Double.parseDouble(elem.getText().trim());

                    if (!(alter<=ele) && (better>=ele))
                    {
                        return false;
                    }
                }
                if (i <= totalPages)
                {
                    waitUntil.fluentWaitMethod(filterMethod.resultsPageElements.getHotelPagination())
                            .findElement(linkText("" + i)).click();
                    waitUntil.explicitWaitUntilPageLoad();
                    Thread.sleep(2000);
                }
            }
            while (i <= totalPages);
        }
        catch (Exception e)
        {
            logger.info(e);
            return false;
        }
        clientDriver.findElement(resultsPageElements.getClear()).click();

        waitUntil.explicitWaitUntilPageLoad();

        return true;
    }
}




