package Data;

import elements.LoginPageElements;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import pages.ResultsPage;
import utility.WaitUntil;

import java.util.List;

public class DataProviderTest
{
    WebDriver clientDriver;

    public WaitUntil waitUntil;

    LoginPageElements loginPageElements = new LoginPageElements();

    static final Logger logger = LogManager.getLogger(DataProviderTest.class);

    @DataProvider (name = "login")
    public static Object[][] DataSet(){

        Object[][] obj = {
                {"standard_user", "23456S$"},
                {"pandianchandru@mailinator.com", "12345S#"}
        };
        return obj;
    }

    public DataProviderTest(WebDriver clientDriver )
    {
        this.clientDriver = clientDriver;
        waitUntil = new WaitUntil(clientDriver);
    }

    public Object[][] readAdvanceSearchPropertyCategory()
    {
        WebElement propertyCategoryButton = findAdvanceSearchButtons("Property Category");

        propertyCategoryButton.click();

        WebElement searchOption = waitUntil.fluentWaitMethodForFindBelow(propertyCategoryButton, loginPageElements.getSearchBox());

        List<WebElement> propertyCategoryLabels = waitUntil
                .fluentWaitMethodForFindBelow(searchOption, By.tagName("fieldset"))
                .findElements(By.tagName("label"));

        int i = 0;
        Object[][] propertyCategoryCheckboxes = new Object[propertyCategoryLabels.size()][1];
        for (WebElement checkbox : propertyCategoryLabels) {
            propertyCategoryCheckboxes[i][0] = checkbox.getText();
            i++;
        }
        closeDriver();
        return propertyCategoryCheckboxes;
    }
    public WebElement findAdvanceSearchButtons( String buttonName)
    {
        List<WebElement> advanceButtons = waitUntil.fluentWaitMethod(loginPageElements.getAdvanceSearchContainer())
                .findElements(By.tagName("button"));

        for (WebElement button : advanceButtons) {
            if (button.getText().equalsIgnoreCase(buttonName))
                return button;
        }
        return null;
    }
    public void closeDriver()
    {
        logger.info("After the Data Provider execution");
        if (clientDriver != null)
            clientDriver.quit();

    }

}
