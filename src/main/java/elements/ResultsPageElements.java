package elements;

import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@Data
public class ResultsPageElements {

    private By seeAvailableRooms = By.xpath("//span[text()='Distance']/following::span[text()='See Available Rooms']");

    private By changeHotel = By.xpath("//p[text()='Change Hotel']");

    private By recommended = By.xpath("//span[text()='Recommended ']");

    private By lowestPrice = By.xpath("//span[text()='(Lowest First)']");

    private By distance = By.xpath("//span[text()='Distance']");

    private By reviewScore = By.xpath("//span[text()='Review Score']");

    private By starRating = By.xpath("//span[text()='Star Rating']");

    private By firstPrice = By.cssSelector(".price_secondary_color.price_view");

    private By hotelNameSearch = By.xpath("//h3[text()='Hotel Name']/following::span[@class='multiselect__placeholder']");

    private By hotelNameSearchInput = By.xpath("input[placeholder='Hotel Name']");

    private By hotelNameLoad = By.cssSelector("div.d-flex.p-3");

    private By clearAllFilters = By.xpath("//a[text()='Clear All Filters']/parent::span");

    private By clear = By.xpath("(//a[contains(.,'Clear All Filters')])[1]");

    private final By getFilterText = By.xpath("//span[@class='selected-filter-label']");

    private By imageValue = By.xpath("//a[@class='rating px-2']");

    private final By moreButton = By.xpath(".//a[contains(.,'More')]");

    private final By amenitiesBox = By.xpath(".//div[@class='d-flex flex-wrap']");

    private By breakFastService = By.xpath("//span[text()='Breakfast service']/parent::div");

    private final By closeButton = By.xpath(".//button[@class='btn custom-close btn-secondary btn-sm']");

    private By library = By.xpath(".//span[text()='Hotelbeds']");

    private By hotelBeds = By.xpath(".//span[text()='Hotelbeds']");

    private By priceLine = By.xpath(".//span[text()='Priceline']");

    private By expedia = By.xpath(".//span[text()='Expedia']");

    private By brands = By.xpath("//p[contains(.,'Brands')]");

    private final String contentSource = "h3:containsOwn(Content Source)";

    private final String distanceFromCenter = "h3:containsOwn(Distance from Center)";

    private final String avgPricePerNight = "h3:containsOwn(Avg. Price Per Night)";

    private final String propertyCategory = "h3:containsOwn(Property Category)";

    private final String themes = "a:containsOwn(Themes/Types)";

    private final String rateTypes = "a:containsOwn(Rate Type)";

    private final String rateCheck = "a:containsOwn(Rate Check)";

    private final String neighBorHood = "a:containsOwn(Neighbourhood)";

    private final String bedPreferences = "a:containsOwn(Bed Perference)";

    private final String hotelAmenities = "a:containsOwn(Hotel Amenities)";

    private final String hotelChains = "a:containsOwn(Hotel Chains & Brands)";

    private final String propertyAccessibility = "a:containsOwn(Property Accessibility)";

    private final String healthAndSafety = "h3:containsOwn(Health & Safety)";

    private final String starRatingFilter = "h3:containsOwn(Star Rating)";

    private By healthAndSafetyIcon = By.xpath(".//i[@title='Health & Safety']");

    private final String popularFilter = "div.tab-content.pt-3";

    private final By hotelContinue = By.xpath("//div[@class='product-list-lft ']");

    private final By freeCancellation = By.xpath("//a[text()='Free Cancellation']");

    private final By miles = By.xpath(".//i[@class='fa fa-map-marker mr-1']/following::span[contains(text(),'miles')]");

    private final By freeBreakFast = By.xpath("//a[text()='Free breakfast']");

    private final By bellByService = By.xpath("//a[text()='Choose Your Room ']/following::span[@class='text-success font-size-14']");

    private final By fiveStar = By.xpath("//i[@class='fa fa-star checked']");

    private final String filterSection = "div.filter-list-option-check.px-2.pt-0";

    private final By priceText = By.cssSelector("span.price_secondary_color.price_view");

    private String avgPrice = "Avg. Price Per Night";

    private final By setOwnBudget = By.xpath(".//p[text()='Set Your Own Budget']");

    private String conditionHotels = ".vue-slider.vue-slider-ltr";

    private By priceRatioSection = By.xpath("//div[@class='price-radio-btn-sec']");

    private final String elementsCheckbox = "fieldset";

    private final String starCheckbox = "li";

    private final By iconStar = By.xpath(".//i[@class='fa fa-star checked']");

    private final String showAllCondition = "a:containsOwn(Show All)";

    private final String expandFilter = "a.collapsed,.filter-list border-bottom py-3 fare-type-list";

    private final By notExpand = By.cssSelector("a.not-collapsed");

    private By hotelPagination = By.cssSelector(".pagination.flight-list-pagination.d-flex");
}
